/**
 * Method not allowed.
 *
 * @return {result} Returns a status 405
 */
export const methodNotAllowed = () => (req, res) => res.status(405).send();
