// Log Color ANSI escape code (https://gist.github.com/RabaDabaDoba/145049536f815903c79944599c6f952a)

export const logColor = {
  bright: '\x1b[1m',
  red: '\x1b[31m',
  white: '\x1b[0m',
  yellow: '\x1b[33m',
  green: '\x1b[32m',
  magenta: '\x1b[35m',
};
