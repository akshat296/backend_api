import { Sequelize } from 'sequelize';

import sequelize from '../config/sequelize';

const tabDef = {
  name: { type: Sequelize.STRING, allowNull: false },
  notes: { type: Sequelize.STRING },
  created_at: {
    type: Sequelize.DATE,
    defaultValue: new Date(),
  },
  updated_at: {
    type: Sequelize.DATE,
    defaultValue: new Date(),
  },
  updated_by_user_id: { type: Sequelize.INTEGER },
  deleted_at: { type: Sequelize.DATE },

};

const Tab = sequelize.define('tab', tabDef);

export { Tab };
