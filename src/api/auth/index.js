import { Router } from 'express';
import AuthController from './auth.controller';
import { Authentication } from '../../middleware';
import { methodNotAllowed } from '../utilities';

const router = new Router();

router
  .post('/login', AuthController.login)
  .post('/forgot-password', AuthController.forgotPassword)
  .post('/logout', Authentication.isAuthenticated, AuthController.forgotPassword)
  .all('/*', methodNotAllowed());

export default router;
