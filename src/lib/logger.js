import { logColor } from '../api/constants';

const logLevels = {
  info: {
    type: 'INFO',
    level: 30,
  },
  error: {
    type: 'ERROR',
    level: 50,
  },
  fatal: {
    type: 'FATAL',
    level: 70,
  },
};

const logger = (config) => (...args) => {
  const { log } = console;
  const type = `${config.type}:`;
  const date = new Date().toLocaleString();
  const seperator = '::';
  const write = Array.of(...args).map((arg) => (typeof arg === 'object' ? JSON.stringify(arg, null, 2) : arg));

  if (config.level < 50) {
    log(type, date, seperator, ...write);
  } else {
    log(logColor.red, type, date, seperator, logColor.white, logColor.bright, ...write, logColor.white);
  }
};

const Logger = {
  info: logger(logLevels.info),
  error: logger(logLevels.error),
  fatal: logger(logLevels.fatal),
};

export default Logger;
