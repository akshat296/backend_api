const config = {
  logLevel: process.env.INFO || 'error',
};

export default config;
