/* eslint-disable import/no-dynamic-require */
require('dotenv').config();

const envConfig = require(`./${process.env.NODE_ENV || 'development'}.js`);

export const config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.API_PORT || 8000,
  baseUrl: process.env.API_URL || 'http://localhost:8000/api',
  ...envConfig || {},
};
