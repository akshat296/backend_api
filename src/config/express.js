import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';

/*
 * Express global middleware and configurations
 */

export default (app) => {
  const corsOptions = {
    allowedHeaders: ['Origin', 'Authorization', 'Content-Type', 'Accept', 'Cache-Control'],
    credentials: true,
    methods: 'GET,OPTIONS,PUT,POST,DELETE',
  };

  app
    .use(cors(corsOptions))
    .use(express.urlencoded({ extended: true }))
    .use(express.json())
    .use(cookieParser())
    .use(morgan('dev'));
};
