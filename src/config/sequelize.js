/* eslint-disable no-param-reassign */
import Sequelize from 'sequelize';

import dbConfig from '../../db';
import { config } from './environment';

const {
  database,
  username,
  password,
  host,
  port,
  dialect,
} = dbConfig[config.env];

const sequelize = new Sequelize(database, username, password, {
  host,
  port,
  dialect,
  dialectOptions: {
    multipleStatements: true,
  },
  pool: {
    max: 5,
    min: 0,
  },
  define: {
    underscored: true,
    paranoid: true,
    timestamps: false,
    hooks: {
      beforeCreate: async (instance, options) => {
        if (!instance.created_by_user_id) {
          const { user } = options;

          instance.created_by_user_id = user ? user.id : null;
          instance.updated_by_user_id = user ? user.id : null;
        }
      },

      beforeUpdate: async (instance, options) => {
        const { user } = options;

        instance.updated_at = new Date();
        instance.updated_by_user_id = user ? user.id : null;
      },

      beforeDestroy: async (instance, options) => {
        const { user } = options;

        instance.deleted_by_user_id = user ? user.id : null;
        instance.is_destroying = true;
        instance.save();
      },
    },
  },
});

export default sequelize;
