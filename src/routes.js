import Auth from './api/auth';

export default (app) => {
  app.use('/api/auth', Auth);
};
