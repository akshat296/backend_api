import express from 'express';
import http from 'http';
import { config } from './config/environment';
import configExpress from './config/express';
import routes from './routes';
import { Logger } from './lib';

const app = express();
const server = http.createServer(app);

configExpress(app);
routes(app);

server.listen(config.port, () => {
  Logger.info(`Server Start on port ${config.port}`);
});
