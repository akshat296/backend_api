module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('tab', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: { type: Sequelize.STRING, allowNull: false },
    notes: { type: Sequelize.STRING },
    created_at: {
      type: Sequelize.DATE,
      defaultValue: new Date(),
    },
    updated_at: {
      type: Sequelize.DATE,
      defaultValue: new Date(),
    },
    updated_by_user_id: { type: Sequelize.INTEGER },
    deleted_at: { type: Sequelize.DATE },
  }),
  down: (queryInterface) => queryInterface.dropTable('tab'),
};
